FROM debian:stretch-slim

EXPOSE 389/tcp

ENV DEBIAN_FRONTEND "noninteractive"

RUN apt-get update \
  && apt-get install -y slapd openssl dumb-init \
  && rm -Rf "/etc/ldap/slapd.d" \
  && apt-get clean

COPY openldap.sh /openldap.sh
RUN chmod +x /openldap.sh

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/openldap.sh"]
