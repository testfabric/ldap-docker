#!/bin/sh
set -e

LDAP_DB=/etc/ldap/slapd.d

if [ ! -d "${LDAP_DB}/cn=config" ]; then
	LDAP_DOMAIN=$(cat /etc/ldap/scripts/domain.ldif | grep 'dn' | head -1 - | awk '{print $2}')
	mkdir -p "${LDAP_DB}"
	slapadd -F "${LDAP_DB}" -n 0 -l /etc/ldap/scripts/slapd.ldif
	slapadd -F "${LDAP_DB}" -b "${LDAP_DOMAIN}" -l /etc/ldap/scripts/domain.ldif
fi

# We have to set a file descriptor limit or openldap hogs memory
# https://github.com/moby/moby/issues/8231
ulimit -n 1024
/usr/sbin/slapd -d stats -F "${LDAP_DB}"
